

<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-book"></i> Book Title <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/atomic_project_panel/views/SEIP142236/BookTitle/create.php"> Add New </a>
                    </li>
                    <li><a href="/atomic_project_panel/views/SEIP142236/BookTitle/list_view.php"> List View </a>
                    </li>
                    <li><a href="/atomic_project_panel/views/SEIP142236/BookTitle/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li>
            <li><a><i class="fa fa-birthday-cake"></i> Birthday <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/atomic_project_panel/views/SEIP142236/Birthday/create.php"> Add New </a>
                    </li>
                    <li><a href="/atomic_project_panel/views/SEIP142236/Birthday/list_view.php"> List View </a>
                    </li>
                    <li><a href="/atomic_project_panel/views/SEIP142236/Birthday/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li>
            <li><a><i class="fa fa-building-o"></i> City <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/atomic_project_panel/views/SEIP142236/City/create.php"> Add New </a>
                    </li>
                    <li><a href="/atomic_project_panel/views/SEIP142236/City/list_view.php"> List View </a>
                    </li>
                    <li><a href="/atomic_project_panel/views/SEIP142236/City/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li>
            <li><a><i class="fa fa-envelope"></i> Email <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/atomic_project_panel/views/SEIP142236/Email/create.php"> Add New </a>
                    </li>
                    <li><a href="/atomic_project_panel/views/SEIP142236/Email/list_view.php"> List View </a>
                    </li>
                    <li><a href="/atomic_project_panel/views/SEIP142236/Email/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li>
            <li><a><i class="fa fa-female"></i> Gender <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/atomic_project_panel/views/SEIP142236/Gender/create.php"> Add New </a>
                    </li>
                    <li><a href="/atomic_project_panel/views/SEIP142236/Gender/list_view.php"> List View </a>
                    </li>
                    <li><a href="/atomic_project_panel/views/SEIP142236/Gender/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li>
            <li><a><i class="fa fa-car"></i> Hobbies <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/atomic_project_panel/views/SEIP142236/Hobbies/create.php"> Add New </a>
                    </li>
                    <li><a href="/atomic_project_panel/views/SEIP142236/Hobbies/list_view.php"> List View </a>
                    </li>
                    <li><a href="/atomic_project_panel/views/SEIP142236/Hobbies/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li>
            <li><a><i class="fa fa-picture-o"></i> Profile Picture <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/atomic_project_panel/views/SEIP142236/ProfilePicture/create.php"> Add New </a>
                    </li>
                    <li><a href="/atomic_project_panel/views/SEIP142236/ProfilePicture/list_view.php"> List View </a>
                    </li>
                    <li><a href="/atomic_project_panel/views/SEIP142236/ProfilePicture/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li>
            <li><a><i class="fa fa-comments"></i> Organization Summary <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="/atomic_project_panel/views/SEIP142236/SummaryOfOrganization/create.php"> Add New </a>
                    </li>
                    <li><a href="/atomic_project_panel/views/SEIP142236/SummaryOfOrganization/list_view.php"> List View </a>
                    </li>
                    <li><a href="/atomic_project_panel/views/SEIP142236/SummaryOfOrganization/trash_view.php"> Trash View </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>

</div>
<!-- /sidebar menu -->

<!-- /menu footer buttons -->
<div class="sidebar-footer hidden-small">
    <a data-toggle="tooltip" data-placement="top" title="Settings">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Lock">
        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Logout">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
    </a>
</div>
<!-- /menu footer buttons -->