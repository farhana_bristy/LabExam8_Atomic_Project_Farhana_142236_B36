<?php

require_once("../../../vendor/autoload.php");
use App\ProfilePicture\ProfilePicture;

$objProfilePicture = new ProfilePicture();
//$_FILES
if($_FILES['profile_photo']['name'])
{
    $file_name = time().$_FILES['profile_photo']['name'];
    $temporary_location = $_FILES['profile_photo']['tmp_name'];

    move_uploaded_file($temporary_location,'../../../picture/'.$file_name);

    $_POST['profile_photo']=$file_name;
}
$objProfilePicture->setData($_POST);
$objProfilePicture->update();