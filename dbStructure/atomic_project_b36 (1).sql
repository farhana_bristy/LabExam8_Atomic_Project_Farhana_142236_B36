-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 19, 2016 at 12:00 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomic_project_b36`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
`birthday_id` int(10) NOT NULL,
  `name` text NOT NULL,
  `birth_date` varchar(100) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`birthday_id`, `name`, `birth_date`, `is_deleted`) VALUES
(1, 'Fariha', '10/12/1991', 0),
(2, 'Emon', '23/10/1998', 0),
(3, 'dsgsdgds', '2016-11-14', 0),
(4, 'rteruteruty', '2016-11-08', 0),
(5, 'gfdgdfg', '2016-11-25', 0),
(6, 'dsdfsfdf', '2016-11-09', 0),
(7, 'jhdfjshdjfhsd', '2016-11-10', 0),
(8, 'trytrytr', '2016-11-30', 0),
(9, 'fdfgdfg', '2016-11-24', 0),
(10, 'ghfghfghfhgf', '2016-11-17', 0),
(11, 'jhjjhhk jhjh', '2016-11-23', 0);

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
`book_id` int(10) NOT NULL,
  `book_title` text NOT NULL,
  `author_name` text NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`book_id`, `book_title`, `author_name`, `is_deleted`) VALUES
(1, 'Satkahon', 'Somoresh Majumder', 1),
(2, 'Moyurakkhii', 'Humayun Ahmed', 1),
(3, 'Himur nil jochona', 'Humayun Ahmed', 0),
(4, 'Kalbela', 'Somoresh Majumder', 0),
(5, 'Aj himur biye', 'Humayun Ahmed', 0),
(6, 'Mrinmoyir mon valo nei', 'Humayun Ahmed', 0),
(7, 'fghfghfgh', 'hfhf hghgffghgf', 0),
(8, 'ami ekta picci', 'Sidratun samia ', 0);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`city_id` int(10) NOT NULL,
  `name` text NOT NULL,
  `city_name` varchar(100) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `name`, `city_name`, `is_deleted`) VALUES
(3, 'Samia', 'Shariatpur', 0),
(4, 'SAJIB', 'Shariatpur', 0),
(5, 'wasif', 'Munsigonj', 0),
(6, 'hfjdhfscnsbgdsgh', 'Noakhali', 0),
(7, 'pori', 'Chittagong', 0);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
`email_id` int(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`email_id`, `email`, `password`, `is_deleted`) VALUES
(1, 'someone@gmail.com', '7ac60b3a8bbe6eb22fc6c129d0b1d9dd', 0),
(2, 'example@yahoo.com', '8e0dfe712821d53026a7ba258bbc970f', 0),
(3, 'bristybilash16@gmail.com', '11f4d0712e8278ac1934f533e51bf6b3', 0),
(4, 'jdsgdfh@yahoo.com', '352751383f8e3dd5f08905fff9ee9a6e', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
`gender_id` int(10) NOT NULL,
  `name` text NOT NULL,
  `gender` varchar(30) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`gender_id`, `name`, `gender`, `is_deleted`) VALUES
(3, 'derwerwe', 'Male', 0),
(4, 'ueyruyer bhdfdghfg', 'Female', 1),
(5, 'ttt', 'Female', 1),
(6, 'himela', 'Female', 0);

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE IF NOT EXISTS `hobbies` (
`hobby_id` int(11) NOT NULL,
  `person_name` varchar(100) NOT NULL,
  `hobbies` varchar(500) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`hobby_id`, `person_name`, `hobbies`, `is_deleted`) VALUES
(1, 'Fariha', 'Book reading,Gardening,Traveling', 0),
(2, 'Emon', 'Traveling', 0),
(7, 'etrye', 'Cycling,Traveling', 1),
(8, 'sabina', 'Novel Reading,Traveling', 0),
(9, 'fdgdfgfdgd', 'Gardening,Cycling,Traveling', 0),
(10, 'farhana', 'Gardening,Swimming,Programming', 0),
(11, 'me me', 'Novel Reading,Cycling,Gardening,Swimming', 0);

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE IF NOT EXISTS `profile_picture` (
`profile_pic_id` int(10) NOT NULL,
  `profile_name` varchar(100) NOT NULL,
  `profile_picture` varchar(300) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`profile_pic_id`, `profile_name`, `profile_picture`, `is_deleted`) VALUES
(6, 'hello', '1479543627image001.png', 0),
(7, 'ttt', '1479491516Tulips.jpg', 0),
(8, 'Samia', '1479542670download.jpg', 0),
(9, '1hjsaghasg', '', 1),
(10, 'me', '1479549482142365.PNG', 0),
(11, 'fairy', '1479549665fairy.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE IF NOT EXISTS `summary_of_organization` (
`summary_organizaton_id` int(10) NOT NULL,
  `organization_name` varchar(100) NOT NULL,
  `summary` text NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`summary_organizaton_id`, `organization_name`, `summary`, `is_deleted`) VALUES
(1, 'BITM', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, ', 0),
(2, 'BASIS', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 0),
(7, 'me', 'lorem ipsum', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
 ADD PRIMARY KEY (`birthday_id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
 ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
 ADD PRIMARY KEY (`email_id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
 ADD PRIMARY KEY (`gender_id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
 ADD PRIMARY KEY (`hobby_id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
 ADD PRIMARY KEY (`profile_pic_id`);

--
-- Indexes for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
 ADD PRIMARY KEY (`summary_organizaton_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
MODIFY `birthday_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
MODIFY `book_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `city_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
MODIFY `email_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
MODIFY `gender_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
MODIFY `hobby_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
MODIFY `profile_pic_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
MODIFY `summary_organizaton_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
