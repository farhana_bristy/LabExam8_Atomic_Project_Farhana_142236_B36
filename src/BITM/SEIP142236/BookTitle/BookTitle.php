<?php

namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class BookTitle extends DB
{
    public $id;
    public $title;
    public $author;

    public function __construct()
    {
        parent::__construct();
        /*if(!isset($_SESSION))
            session_start();*/
    }

    public function setData($postVaribaleData=NULL)
    {
       if(array_key_exists("id",$postVaribaleData))
       {
           $this->id = $postVaribaleData['id'];
       }
        if(array_key_exists("book_title",$postVaribaleData))
        {
            $this->title = $postVaribaleData['book_title'];
        }
        if(array_key_exists("author_name",$postVaribaleData))
        {
            $this->author = $postVaribaleData['author_name'];
        }

    }//end of set data
  /*  public function store()   //basic code
    {
        $sql = "INSERT into book_title(book_title,author_name) VALUES ('$this->title','$this->author')";
        $STH = $this->dbh->prepare($sql);
        $STH->execute();
    }//end of store*/

    public function store()
    {
        $arrData = array($this->title,$this->author);
        $sql = "INSERT into book_title(book_title,author_name) VALUES (?,?)";
        $STH = $this->dbh->prepare($sql);
        $result = $STH->execute($arrData);

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('create.php');

    }

    public function index($fetchMode='ASSOC'){

        $STH = $this->dbh->query('SELECT * from book_title WHERE is_deleted =0 ORDER BY book_id ASC');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function view($fetchMode='ASSOC'){

        $sql = 'SELECT * from book_title where book_id='.$this->id ;

        $STH = $this->dbh->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();
    public function update()
    {
        $arrData = array($this->title,$this->author);
        $sql = "UPDATE book_title SET book_title = ?,author_name = ? WHERE book_id = ".$this->id;
        $STH = $this->dbh->prepare($sql);
        $result = $STH->execute($arrData);

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('list_view.php');

    }//end of update
    public function trash()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "UPDATE book_title SET is_deleted = 1 WHERE book_id = ".$this->id;
        $STH = $this->dbh->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('list_view.php');

    }//end of trash

    public function trash_view($fetchMode='ASSOC'){

        $STH = $this->dbh->query('SELECT * from book_title WHERE is_deleted =1 ORDER BY book_id DESC');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashview();
    public function recover()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "UPDATE book_title SET is_deleted = 0 WHERE book_id = ".$this->id;
        $STH = $this->dbh->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('trash_view.php');

    }//end of trash
    public function delete()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "delete from book_title WHERE book_id = ".$this->id;
        $STH = $this->dbh->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('list_view.php');

    }//end of trash
    public function delete_from_trash()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "delete from book_title WHERE book_id = ".$this->id;
        $STH = $this->dbh->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('trash_view.php');

    }//end of trash
}//end of book title class