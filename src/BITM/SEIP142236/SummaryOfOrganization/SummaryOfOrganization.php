<?php

namespace App\SummaryOfOrganization;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;



class SummaryOfOrganization extends DB
{
    public $id;
    public $name;
    public $summary;

    public function __construct()
    {
        parent::__construct();
      /*  if(!isset($_SESSION))
            session_start();*/
    }
    public function setData($postVaribaleData=NULL)
    {
        if(array_key_exists("id",$postVaribaleData))
        {
            $this->id = $postVaribaleData['id'];
        }
        if(array_key_exists("name",$postVaribaleData))
        {
            $this->name = $postVaribaleData['name'];
        }
        if(array_key_exists("summary",$postVaribaleData))
        {
            $this->summary = $postVaribaleData['summary'];
        }

    }//end of set data
    public function store()
    {
        $arrData = array($this->name,$this->summary);
        $sql = "INSERT into summary_of_organization(organization_name,summary) VALUES (?,?)";
        $STH = $this->dbh->prepare($sql);
        $result = $STH->execute($arrData);

        if($result) {
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        }
        else {
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");
        }
            Utility::redirect('create.php');


    }
    public function index($fetchMode='ASSOC'){

        $STH = $this->dbh->query('SELECT * from summary_of_organization WHERE is_deleted = 0 ORDER BY summary_organizaton_id DESC');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function view($fetchMode='ASSOC'){

        $sql = 'SELECT * from summary_of_organization where summary_organizaton_id='.$this->id;

        $STH = $this->dbh->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();
    public function update()
    {
        $arrData = array($this->name,$this->summary);
        $sql = "UPDATE summary_of_organization SET organization_name = ?,summary = ? WHERE summary_organizaton_id = ".$this->id;
        $STH = $this->dbh->prepare($sql);
        $result = $STH->execute($arrData);

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('list_view.php');

    }//end of update
    public function delete()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "delete from summary_of_organization WHERE summary_organizaton_id = ".$this->id;
        $STH = $this->dbh->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('list_view.php');

    }//end of trash
    public function trash()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "UPDATE summary_of_organization SET is_deleted = 1 WHERE summary_organizaton_id = ".$this->id;
        $STH = $this->dbh->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('list_view.php');

    }//end of trash

    public function trash_view($fetchMode='ASSOC'){

        $STH = $this->dbh->query('SELECT * from summary_of_organization WHERE is_deleted =1 ORDER BY summary_organizaton_id DESC');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashview();
    public function recover()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "UPDATE summary_of_organization SET is_deleted = 0 WHERE summary_organizaton_id = ".$this->id;
        $STH = $this->dbh->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('trash_view.php');

    }//end of trash
    public function delete_from_trash()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "delete from summary_of_organization WHERE summary_organizaton_id = ".$this->id;
        $STH = $this->dbh->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('trash_view.php');

    }//end of trash
}