<?php

namespace App\Hobbies;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class Hobbies extends DB
{
    public $id;
    public $name;
    public $hobbies;
    public $hobby;


    public function __construct()
    {
        parent::__construct();
       /* if(!isset($_SESSION))
            session_start();*/
    }
    public function setData($postVaribaleData=NULL)
    {


        if(array_key_exists("id",$postVaribaleData))
        {
            $this->id = $postVaribaleData['id'];
        }
        if(array_key_exists("name",$postVaribaleData))
        {
            $this->name = $postVaribaleData['name'];
        }
        /*for ($i=1;$i<=4;$i++)
        {
            if(array_key_exists("hobby".$i,$postVaribaleData))
            {
                $this->hobby = $postVaribaleData['hobby'.$i];
                array_push($this->hobbies,$this->hobby);
            }
        }

        $this->hobby2=implode(',',$this->hobbies);*/        //my code

        if(array_key_exists("hobby",$postVaribaleData))
        {
           $this->hobby = $postVaribaleData['hobby'];
        }
    }//end of set data
    public function store()
    {
        $arrData = array($this->name,$this->hobby);
        $sql = "INSERT into hobbies(person_name,hobbies) VALUES (?,?)";
        $STH = $this->dbh->prepare($sql);
        $result = $STH->execute($arrData);

        /*if($result) {
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        }
        else {
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");
        }*/
        Utility::redirect('create.php');


    }
    public function index($fetchMode='ASSOC'){

        $STH = $this->dbh->query('SELECT * from hobbies WHERE is_deleted =0 ORDER BY hobby_id DESC');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function view($fetchMode='ASSOC'){

        $sql = 'SELECT * from hobbies where hobby_id='.$this->id;

        $STH = $this->dbh->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();
    public function update()
    {
        $arrData = array($this->name,$this->hobby);
        $sql = "UPDATE hobbies SET person_name = ?,hobbies = ? WHERE hobby_id = ".$this->id;
        $STH = $this->dbh->prepare($sql);
        $result = $STH->execute($arrData);

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('list_view.php');

    }//end of update

    public function delete()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "delete from hobbies WHERE hobby_id = ".$this->id;
        $STH = $this->dbh->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('list_view.php');

    }//end of trash
    public function trash()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "UPDATE hobbies SET is_deleted = 1 WHERE hobby_id = ".$this->id;
        $STH = $this->dbh->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('list_view.php');

    }//end of trash

    public function trash_view($fetchMode='ASSOC'){

        $STH = $this->dbh->query('SELECT * from hobbies WHERE is_deleted =1 ORDER BY hobby_id DESC');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashview();

    public function delete_from_trash()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "delete from hobbies WHERE hobby_id = ".$this->id;
        $STH = $this->dbh->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('trash_view.php');

    }//end of delete_from_trash

    public function recover()
    {
        //$date = date('d/m/Y h:i:s');
        $sql = "UPDATE hobbies SET is_deleted = 0 WHERE hobby_id = ".$this->id;
        $STH = $this->dbh->prepare($sql);
        $STH->execute();

        /*if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");*/

        Utility::redirect('trash_view.php');

    }//end of recover

}