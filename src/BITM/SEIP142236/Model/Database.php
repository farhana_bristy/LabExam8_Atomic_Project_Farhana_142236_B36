<?php
namespace App\Model;

use PDO;
use PDOException;




class Database
{

    public $dbh;

    public $username = "root";

    public $password = "";


    public function __construct()

    {

        try {


            $this->dbh = new PDO("mysql:host=localhost;dbname=atomic_project_b36", $this->username, $this->password);
           // echo "connection successfull";

        } catch (PDOException $e) {

            echo $e->getMessage();
        }

    }


}